import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:workiom/src/core/base/base_state.dart';
import 'package:workiom/src/core/config/theme/app_colors.dart';
import 'package:workiom/src/core/utils/routes/extensions.dart';
import 'package:workiom/src/modules/dashboard/presentation/components/chart_component.dart';

import '../../domain/models/dashboard_model.dart';
import '../components/dash_board_appbar.dart';
import '../manager/dashboard_controller.dart';

class DashboardScreen extends GetView<DashboardController> {
  const DashboardScreen({super.key});
  static const String route = "/";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        body: RefreshIndicator(
          onRefresh: () async => controller.fetchCurrentDashboard(),
          child: ListView(
              padding: EdgeInsets.only(left: 2.gw, right: 2.gw),
              children: [
                14.sizedH,
                const DashboardAppBar(),
                Padding(
                  padding: EdgeInsets.all(16.gw),
                  child: GetBuilder<DashboardController>(
                      init: controller,
                      builder: (_) {
                        return switch (controller.getState.runtimeType) {
                          ErrorState => Center(
                                  child: Text(controller.errMsg,
                                      style: TextStyle(
                                          color: AppColors.black,
                                          fontSize: 20)))
                              .sized(height: Get.height),
                          LoadingState => const Center(
                              child:
                                  CircularProgressIndicator(strokeWidth: 0.8),
                            ).sized(height: Get.height),
                          _ => LinearChart(
                              details: (controller.getState as LoadedState<
                                      DashboardWidgetDetailsModel>)
                                  .data!,
                              label: "Deals per assignee/Status",
                            ),
                        };
                      }),
                )
              ]),
        ),
      ),
    );
  }
}
