import 'package:workiom/src/core/base/base_controller.dart';
import 'package:workiom/src/modules/dashboard/domain/repo/dashboard_repo.dart';

import '../../../../core/base/base_state.dart';
import '../../domain/models/dashboard_model.dart';

class DashboardController extends BaseController<DashboardWidgetDetailsModel> {
  final DashboardRepo dashboardRepo;
  DashboardController({required this.dashboardRepo});

  void fetchCurrentDashboard() async {
    setState = LoadingState();
    final either = await dashboardRepo.fetchDashboard();
    either.fold((l) => setState = ErrorState(l.message, type: l.type!),
        (r) => setState = LoadedState<DashboardWidgetDetailsModel>(data: r));

    update();
  }

  @override
  void onInit() {
    fetchCurrentDashboard();
    super.onInit();
  }
}
