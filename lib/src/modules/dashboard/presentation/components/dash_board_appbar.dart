import 'package:flutter/material.dart';
import 'package:workiom/src/core/utils/routes/extensions.dart';

import '../../../../core/config/size/size_utils.dart';

class DashboardAppBar extends StatelessWidget {
  const DashboardAppBar({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(children: [
      Expanded(
          child: SizedBox(
              width: 90.gw,
              child: Row(
                children: [
                  Icon(
                    Icons.arrow_back_ios_new,
                    size: 20.gh,
                    color: const Color(0xff555555),
                  ),
                  2.sizedW,
                  Expanded(
                    child: Text("Work..",
                        style: TextStyle(
                            color: const Color(0xff555555),
                            fontSize: getFontSize(17))),
                  )
                ],
              ))),
      Expanded(
          child: Center(
        child: Text("Task Progress",
            textAlign: TextAlign.center,
            style: TextStyle(
                fontWeight: FontWeight.w400,
                color: const Color(0xff0e0f12),
                fontSize: getFontSize(17))),
      )),
      const Expanded(child: SizedBox()),
    ]);
  }
}
