// 10 / 7
// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_animate/flutter_animate.dart';
import 'package:get/get.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:workiom/src/core/utils/routes/extensions.dart';

import '../../../../core/config/size/size_utils.dart';
import '../../../../core/config/theme/app_colors.dart';
import '../../../../core/utils/functions.dart';
import '../../domain/models/dashboard_model.dart';

class LinearChart extends StatelessWidget {
  LinearChart({super.key, required this.details, required this.label});
  final DashboardWidgetDetailsModel details;
  final String label;

  OverlayEntry? overlayEntry;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 8.gh),
      width: (343).gw,
      decoration: BoxDecoration(
          color: const Color(0xffF4F4F4),
          borderRadius: BorderRadius.circular(18)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.all(16.gh).copyWith(bottom: 0),
            child: Text(label,
                textAlign: TextAlign.start,
                maxLines: 2,
                style: TextStyle(
                    color: AppColors.prim,
                    fontSize: getFontSize(17),
                    fontWeight: FontWeight.w500)),
          ),
          // 5.sizedH,
          Column(
            children: [
              ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.all(16.gh).copyWith(bottom: 0, top: 0),
                  itemCount: details.mainGroupLabelValues?.length,
                  physics: const NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    String? id = details.mainGroupLabelValues![index].value!.id;

                    return Padding(
                      padding: EdgeInsets.only(top: index != 0 ? 8.0 : 0),
                      child: Row(
                        children: [
                          Text(details.mainGroupLabelValues?[index].key ?? "",
                              style: TextStyle(
                                fontSize: getFontSize(12),
                                color: const Color(0xff0E0F12),
                              )).sized(width: Get.width * 0.27),
                          SizedBox(
                            width: Get.width * 0.54,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(2),
                              child: Row(
                                children: [
                                  for (var i = 0;
                                      i < details.values!.length;
                                      i++)
                                    for (var j = 0;
                                        j < details.values![i].value!.length;
                                        j++)
                                      Visibility(
                                        replacement: details.values![i]
                                                    .value![j].value !=
                                                0
                                            ? 30.sizedH
                                            : 0.sizedH,
                                        visible: details.values![i].value![j]
                                                    .value !=
                                                0 &&
                                            details.values![i].value![j].ids!
                                                .contains(id),
                                        child: Expanded(
                                          flex: details
                                                  .values![i].value![j].value
                                                  .toInt() ??
                                              1,
                                          child: GestureDetector(
                                            onTapDown: (detail) {
                                              _showLabelOverlay(
                                                  context,
                                                  detail.globalPosition,
                                                  HexColor(details
                                                          .values![i].color ??
                                                      "#555555"),
                                                  details.values![i].value![j]
                                                      .value
                                                      .toString());
                                            },
                                            onTapUp: (_) {
                                              _removeLabelOverlay();
                                            },
                                            onTapCancel: () {
                                              _removeLabelOverlay();
                                            },
                                            child: Container(
                                              margin: EdgeInsets.only(
                                                  left: i == 0 ? 2 : 0),
                                              width: Get.width,
                                              height: 30,
                                              decoration: BoxDecoration(
                                                color: HexColor(
                                                    details.values![i].color ??
                                                        "#555555"),
                                              ),
                                            ).animate().scaleX(
                                                duration: const Duration(
                                                    milliseconds: 300),
                                                begin: 0,
                                                alignment: Alignment.centerLeft,
                                                end: 1),
                                          ),
                                        ),
                                      ),
                                  Expanded(
                                      flex: getMaxNumberOfDashboards(details)
                                          .toInt(),
                                      child: const SizedBox())
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }),
              10.sizedH,
              Wrap(
                children: [
                  for (int i = 0; i < details.values!.length; i++)
                    Padding(
                      padding: const EdgeInsets.only(top: 8),
                      child: SizedBox(
                        width: Get.width * 0.27,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: getVerticalSize(12),
                              height: getVerticalSize(12),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: HexColor(
                                      details.values![i].color ?? "#555555")
                                  //  AppMapColors.getColorByString(
                                  //
                                  ),
                            ),
                            Flexible(
                              // width: Get.width * 0.2,
                              child: Text(
                                ' ${details.values![i].label ?? ''}',
                                textAlign: TextAlign.start,
                                style: TextStyle(
                                    fontSize: getFontSize(15),
                                    color: const Color(0xff0E0F12)),
                              ),
                            ),
                            2.sizedW,
                            Text(
                              getNumberOfValue(details, i).toString(),
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: getFontSize(16),
                                  color: const Color(0xff747474)),
                            ),
                          ],
                        ),
                      ),
                    )
                ],
              ),
              20.sizedH,
              const Divider(
                color: Colors.white,
                thickness: 2,
              ),
              10.sizedH,
              Container(
                padding: EdgeInsets.all(16.gh).copyWith(bottom: 0, top: 0),
                child: Row(
                  children: [
                    Container(
                      width: 7.gh,
                      height: 7.gh,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(2),
                          color: const Color.fromRGBO(215, 70, 112, 2)),
                    ),
                    10.sizedW,
                    Text(
                      "Sales / Reports",
                      style: TextStyle(
                          color: const Color(0xff555555), fontSize: 12.gh),
                    )
                  ],
                ),
              ),
              12.sizedH,
            ],
          ),
        ],
      ),
    );
  }

  void _showLabelOverlay(
      BuildContext context, Offset position, Color color, String label) {
    overlayEntry = OverlayEntry(
      builder: (context) => Positioned(
        top: position.dy - 50,
        left: position.dx - 50,
        child: Material(
          color: Colors.transparent,
          child: Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.black,
              borderRadius: BorderRadius.circular(8),
            ),
            child: Row(
              children: [
                CircleAvatar(backgroundColor: color, radius: 7),
                5.sizedW,
                Text(
                  label,
                  style: const TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

    Overlay.of(context).insert(overlayEntry!);
  }

  void _removeLabelOverlay() {
    overlayEntry?.remove();
  }
}
