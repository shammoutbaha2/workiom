import 'package:dartz/dartz.dart';
import 'package:workiom/src/core/base/base_error_model.dart';
import 'package:workiom/src/core/base/base_handler.dart';
import 'package:workiom/src/modules/dashboard/domain/models/dashboard_model.dart';

import '../../../core/base/base_api.dart';
import '../domain/repo/dashboard_repo.dart';

class DashboardRepoImpl extends DashboardRepo {
  final BaseApi baseApi;

  DashboardRepoImpl(this.baseApi);

  @override
  Future<Either<ErrorModel, DashboardWidgetDetailsModel>> fetchDashboard() {
    return BaseHandler.execute<DashboardWidgetDetailsModel>(
        () => baseApi.post(
                '/services/app/AppDashboard/Preview?appId=c1033f87-6f23-4605-93df-1e2b07d1bc56',
                body: {
                  "appId": "c1033f87-6f23-4605-93df-1e2b07d1bc56",
                  "label": "Deals per assignee/Status",
                  "icon": "fal fa-abacus",
                  "listId": "d9bb38df-bb7f-4491-8ac4-393200c8dffc",
                  "viewId": null,
                  "filters": [],
                  "filterCollectionOperator": 0,
                  "summaryMode": null,
                  "widgetType": 3,
                  "valueFieldId": [2209594],
                  "fieldAggregationType": 0,
                  "groupingFieldId": 2209400,
                  "groupingDateFrequency": 2,
                  "subGroupingFieldId": 2209393,
                  "subGroupingDateFrequency": 2,
                  "isPin": true,
                  "sortingType": 0,
                  "sortingType2": 0,
                  "target": null,
                  "limit": 100,
                  "limitSub": 100,
                  "flipSides": false
                }), onRight: (data) {
      return DashboardWidgetDetailsModel.fromJson(data['result']);
    });
  }
}
