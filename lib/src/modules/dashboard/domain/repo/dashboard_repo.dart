import 'package:dartz/dartz.dart';

import '../../../../core/base/base_error_model.dart';
import '../models/dashboard_model.dart';

abstract class DashboardRepo {
  Future<Either<ErrorModel, DashboardWidgetDetailsModel>> fetchDashboard();
}
