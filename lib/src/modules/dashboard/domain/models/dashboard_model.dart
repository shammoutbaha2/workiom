import 'dart:convert';

class DashboardWidgetDetailsModel {
  final bool? isValid;
  final String? groupByLabel;
  final num? groupByFieldId;
  final String? subGroupByLabel;
  final num? subGroupByFieldId;
  final List<ResultValue>? values;
  final List<String>? mainGroupLabels;
  final List<MainGroupLabelValue>? mainGroupLabelValues;

  DashboardWidgetDetailsModel({
    this.isValid,
    this.groupByLabel,
    this.groupByFieldId,
    this.subGroupByLabel,
    this.subGroupByFieldId,
    this.values,
    this.mainGroupLabels,
    this.mainGroupLabelValues,
  });

  factory DashboardWidgetDetailsModel.fromJson(Map<String, dynamic> json) {
    return DashboardWidgetDetailsModel(
      isValid: json["isValid"],
      groupByLabel: json["groupByLabel"],
      groupByFieldId: json["groupByFieldId"],
      subGroupByLabel: json["subGroupByLabel"],
      subGroupByFieldId: json["subGroupByFieldId"],
      values: json["values"] == null
          ? []
          : List<ResultValue>.from(
              json["values"]!.map((x) => ResultValue.fromJson(x))),
      mainGroupLabels: json["mainGroupLabels"] == null
          ? []
          : List<String>.from(json["mainGroupLabels"]!.map((x) => x)),
      mainGroupLabelValues: json["mainGroupLabelValues"] == null ||
              json["mainGroupLabelValues"] is String
          ? []
          : List<MainGroupLabelValue>.from(json["mainGroupLabelValues"]!
              .map((x) => MainGroupLabelValue.fromJson(x))),
    );
  }
}

class MainGroupLabelValue {
  final String? key;
  final LabelValueElement? value;

  MainGroupLabelValue({
    this.key,
    this.value,
  });

  factory MainGroupLabelValue.fromJson(Map<String, dynamic> json) {
    return MainGroupLabelValue(
      key: json["key"],
      value: LabelValueElement.fromJson(json["value"] ?? {}),
    );
  }
}

class LabelValueElement {
  final String? id;
  final String? label;

  LabelValueElement({
    this.id,
    this.label,
  });

  factory LabelValueElement.fromRawJson(String str) =>
      LabelValueElement.fromJson(json.decode(str));

  factory LabelValueElement.fromJson(Map<String, dynamic> json) {
    return LabelValueElement(
      id: json["id"] ?? "",
      label: json["label"] ?? "",
    );
  }
}

class ResultValue {
  final String? label;
  final List? value;
  final String? color;
  final LabelValue? labelValue;
  final List<String>? ids;

  ResultValue({
    this.label,
    this.value,
    this.color,
    this.labelValue,
    this.ids,
  });

  factory ResultValue.fromRawJson(String str) =>
      ResultValue.fromJson(json.decode(str));

  factory ResultValue.fromJson(Map<String, dynamic> json) {
    return ResultValue(
      label: json["label"],
      value: json["value"] is List
          ? List.from(json["value"]!.map((x) => PurpleValue.fromJson(x)))
          : [json["value"]],
      color: json["color"],
      labelValue: json["labelValue"] is Map
          ? LabelValue.fromJson(json["labelValue"])
          : null,
      ids: json["ids"] == null
          ? []
          : List<String>.from(json["ids"]!.map((x) => x)),
    );
  }
}

class LabelValue {
  final String? id;
  final String? label;
  final num? order;
  final String? color;
  final bool? isClosed;

  LabelValue({
    this.id,
    this.label,
    this.order,
    this.color,
    this.isClosed,
  });

  factory LabelValue.fromJson(Map<String, dynamic> json) {
    return LabelValue(
      id: json["id"],
      label: json["label"],
      color: json["color"],
      isClosed: json["isClosed"],
    );
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "label": label,
        "order": order,
        "color": color,
        "isClosed": isClosed,
      };
}

class PurpleValue {
  final String? label;
  final num? value;
  final String? color;
  final List<LabelValueElement>? labelValue;
  final List<String>? ids;

  PurpleValue({
    this.label,
    this.value,
    this.color,
    this.labelValue,
    this.ids,
  });

  factory PurpleValue.fromJson(Map<String, dynamic> json) {
    return PurpleValue(
      label: json["label"],
      value: json["value"],
      color: json["color"],
      labelValue: json["labelValue"] is List
          ? List<LabelValueElement>.from(
              json["labelValue"]!.map((x) => LabelValueElement.fromJson(x)))
          : [],
      ids: json["ids"] == null
          ? []
          : List<String>.from(json["ids"]!.map((x) => x)),
    );
  }
}
