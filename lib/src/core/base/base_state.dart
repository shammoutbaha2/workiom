abstract class AppState<T> {}

class InitialState<T> extends AppState {}

class LoadingState extends AppState {}

class PaginationLoadingState extends AppState {}

class LoadedState<T> extends AppState {
  final T? data;
  final Function? onIt;
  LoadedState({this.data, this.onIt});
}

class PaginationDoneState extends AppState {}

class PaginationLoadedState<T> extends LoadedState {
  PaginationLoadedState({super.data});
}

class ErrorState extends AppState {
  final String error, type;
  final Function? onIt;

  ErrorState(this.error, {this.type = '' , this.onIt});
}

class PaginationErrorState extends AppState {
  final String error;

  PaginationErrorState(this.error);
}
