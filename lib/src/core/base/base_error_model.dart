class ErrorModel {
  final String? type;
  final String message;
  final int? code;

  ErrorModel(this.message, {this.type, this.code});

  Map toJson() => {"type": type, "message": message, "code": code};
}
