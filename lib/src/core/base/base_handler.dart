import 'dart:developer';

import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../base/base_error_model.dart';
import '../base/base_state.dart';
import '../utils/check_connection.dart';
import '../utils/errors/error_constant.dart';
import '../utils/strings.dart';
import 'base_api_response.dart';

class BaseHandler {
  static bool isLoaderShowing = false;
  static void state(AppState state) {
    switch (state.runtimeType) {
      case LoadingState:
        return showLoader();
      case LoadedState:
        LoadedState ls = state as LoadedState;
        closeLoader();
        if (ls.onIt != null) {
          ls.onIt!();
        } else {
          Get.showSnackbar(GetSnackBar(title: "Success"));
        }
        return;
      case ErrorState:
        closeLoader();
        ErrorState es = state as ErrorState;
        if (es.onIt != null) {
          es.onIt!();
        } else {
          Get.showSnackbar(GetSnackBar(title: es.type, message: es.error));
        }
    }
  }

  static void closeLoader() {
    if (isLoaderShowing) Get.close(1);
    isLoaderShowing = false;
  }

  static void showLoader() {
    if (isLoaderShowing) return;
    isLoaderShowing = true;
    Get.closeAllSnackbars();
    Get.dialog(WillPopScope(
        onWillPop: () async {
          return true;
        },
        child: const Center(
            child: CircularProgressIndicator(
          strokeWidth: .2,
        ))));
  }

  static Future<Either<ErrorModel, S>> execute<S>(
      Future<ResponseModel> Function() func,
      {required S Function(dynamic data) onRight}) async {
    try {
      final res = await func();
      if (res.fail) return Left(res.errorModel!);

      return Right(onRight(res.data));
    } catch (e) {
      return Left(ErrorModel(e.toString(), type: StringKeys.UNKNOWN_ERROR));
    }
  }

  static Future<ResponseModel> executeRemote(
      Future<dio.Response<dynamic>> Function() func) async {
    if (await noConnection()) {
      return ErrorsConstantModel.NO_INTERNET_MODEL_ERROR;
    }
    try {
      final response = await func();
      if (response.statusCode == 200 || response.statusCode == 201) {
        return ResponseModel(data: response.data);
      } else {
        throw StringKeys.AN_ERROR_OCCURRED;
      }
    } on dio.DioException catch (e) {
      log(e.toString());
      return ErrorsConstantModel.DIO_MODEL_ERROR(e);
    } catch (e) {
      return ErrorsConstantModel.GENERAL_MODEL_ERROR(e);
    }
  }
}
