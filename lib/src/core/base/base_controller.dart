import 'package:get/get.dart';

import 'base_state.dart';

class BaseController<T> extends GetxController {
  AppState _state = InitialState();

  set setState(AppState state) {
    _state = state;
    update();
  }

  AppState get getState => _state;

  String message = '';

  String get errMsg =>
      getState is ErrorState ? (getState as ErrorState).error : "";

  T get data => (getState.runtimeType as LoadedState).data;
}
