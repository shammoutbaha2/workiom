 
import 'base_error_model.dart';

class ResponseModel {
  dynamic data;
  bool get success => errorModel == null;
  bool get fail => !success;
  ErrorModel? errorModel;
  int code;

  ResponseModel({this.data, this.code = 0, this.errorModel});

  Map toJson() => {
        "data": data,
        "success": success,
        "errorModel": errorModel?.toJson(),
      };

  @override
  String toString() => toJson().toString();
}
