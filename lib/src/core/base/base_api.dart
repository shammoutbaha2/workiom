import '../base/base_api_response.dart';
import '../base/base_handler.dart';
import '../config/helper/dio_helper.dart';
 
class BaseApi {
  final DioHelper dioHelper;
  BaseApi(this.dioHelper);

  Future<ResponseModel> get(String path,
      {Map<String, dynamic>? queryParameters,
      Map<String, String>? extraHeaders,
      bool auth = true}) async {
    return await BaseHandler.executeRemote(() => dioHelper.get(
        path: path,
        queryParameters: queryParameters,
        extraHeaders: extraHeaders,
        auth: auth));
  }

  Future<ResponseModel> post(
    String path, {
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  }) async {
    return await BaseHandler.executeRemote(() => dioHelper.post(
        path: path,
        body: body,
        queryParameters: queryParameters,
        extraHeaders: extraHeaders,
        hasFile: hasFile,
        auth: auth));
  }

  Future<ResponseModel> update(
    String path, {
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  }) async {
    return await BaseHandler.executeRemote(() => dioHelper.update(
        path: path,
        body: body,
        queryParameters: queryParameters,
        extraHeaders: extraHeaders,
        hasFile: hasFile,
        auth: auth));
  }

  Future<ResponseModel> delete(String path,
      {Map<String, dynamic>? body,
      Map<String, dynamic>? queryParameters,
      Map<String, String>? extraHeaders,
      bool auth = true}) async {
    return await BaseHandler.executeRemote(() => dioHelper.delete(
        path: path,
        body: body,
        queryParameters: queryParameters,
        extraHeaders: extraHeaders,
        auth: auth));
  }
}
