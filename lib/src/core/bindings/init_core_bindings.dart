import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:workiom/src/core/base/base_api.dart';
import 'package:workiom/src/core/config/helper/dio_helper.dart';

class InitCoreBindings {
  static void init() {
    Get.lazyPut<Dio>(() => Dio(), fenix: true);
    Get.lazyPut<DioHelper>(() => DioHelperImpl(client: Get.find<Dio>()),
        fenix: true);
    Get.lazyPut<BaseApi>(() => BaseApi(Get.find<DioHelper>()), fenix: true);
  }
}
