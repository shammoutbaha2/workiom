import 'package:get/get.dart';
import 'package:workiom/src/core/bindings/init_core_bindings.dart';

import 'dashboard_bindings.dart';

class AppBindings extends Bindings {
  @override
  void dependencies() {
    InitCoreBindings.init();
    DashboardBindings.init();
  }
}
