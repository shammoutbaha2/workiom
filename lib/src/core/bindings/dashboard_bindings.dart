import 'package:get/get.dart';
import 'package:workiom/src/core/base/base_api.dart';
import 'package:workiom/src/modules/dashboard/data/dashboard_repo_impl.dart';
import 'package:workiom/src/modules/dashboard/domain/repo/dashboard_repo.dart';
import 'package:workiom/src/modules/dashboard/presentation/manager/dashboard_controller.dart';

class DashboardBindings {
  static void init() {
    Get.lazyPut<DashboardRepo>(() => DashboardRepoImpl(Get.find<BaseApi>()));
    Get.lazyPut(
        () => DashboardController(dashboardRepo: Get.find<DashboardRepo>()),
        fenix: true);
  }
}
