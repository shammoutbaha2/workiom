import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

@immutable
class AppTheme {
  static ThemeData base() {
    return ThemeData(
        textTheme: GoogleFonts.poppinsTextTheme(),
        useMaterial3: true,
        scaffoldBackgroundColor:
            Get.isDarkMode ? const Color(0xFF2B335B) : Colors.white,
        colorScheme: Get.isDarkMode ? darkColorScheme : lightColorScheme);
  }

  static ThemeData get getColor {
    return base();
  }

  static ColorScheme get getColorScheme {
    return base().colorScheme;
  }

  static final lightColorScheme = ColorScheme.light(
    primary: const Color(0xFF2B335B),
    onPrimary: Colors.white,
    primaryContainer: const Color(0xFF2B335B),
    onPrimaryContainer: Colors.white,
    secondary: const Color(0xFF017EFA),
    onSecondary: Colors.white,
    secondaryContainer: const Color(0xFF017EFA),
    onSecondaryContainer: Colors.white,
    tertiary: const Color(0xFFF1F1F1),
    onTertiary: Colors.black,
    tertiaryContainer: const Color(0xFFF1F1F1),
    onTertiaryContainer: Colors.black,
    error: Colors.red,
    onError: Colors.white,
    errorContainer: const Color.fromARGB(255, 198, 54, 43),
    onErrorContainer: Colors.white,
    background: const Color(0xFFFFFFFF),
    onBackground: Colors.black,
    surface: const Color(0xFFF1F1F1),
    onSurface: Colors.black,
    surfaceVariant: Colors.white,
    onSurfaceVariant: Colors.black,
    outline: Colors.transparent,
    shadow: Colors.black.withOpacity(0.2),
    inverseSurface: Colors.white,
    onInverseSurface: Colors.black,
    inversePrimary: const Color(0xFF2B335B),
    surfaceTint: Colors.white,
  );

  static final darkColorScheme = ColorScheme.dark(
    brightness: Brightness.dark,
    primary: const Color(0xFF2B335B),
    onPrimary: Colors.white,
    primaryContainer: const Color(0xFF2B335B),
    onPrimaryContainer: Colors.white,
    secondary: const Color(0xFF017EFA),
    onSecondary: Colors.white,
    secondaryContainer: const Color(0xFF017EFA),
    onSecondaryContainer: Colors.white,
    background: const Color(0xFF1F1F1F),
    onBackground: Colors.white,
    surface: const Color(0xFF1F1F1F),
    onSurface: Colors.white,
    surfaceVariant: const Color(0xFF2B335B),
    onSurfaceVariant: Colors.white,
    outline: Colors.transparent,
    shadow: Colors.white.withOpacity(0.2),
    tertiary: const Color(0xFFF1F1F1),
    onTertiary: Colors.black,
    tertiaryContainer: const Color(0xFFF1F1F1),
    onTertiaryContainer: Colors.black,
    error: Colors.red,
    onError: Colors.white,
    errorContainer: Colors.red,
    onErrorContainer: Colors.white,
    inverseSurface: Colors.white,
    onInverseSurface: Colors.black,
    inversePrimary: const Color(0xFF2B335B),
    surfaceTint: Colors.white,
  );
}
