import 'package:flutter/material.dart';

import 'theme.dart';

class AppColors {
  static Color prim = AppTheme.getColorScheme.primary;
  static Color second = AppTheme.getColorScheme.secondary;
  static Color onprim = AppTheme.getColorScheme.onPrimary;
  static Color onscond = AppTheme.getColorScheme.onSecondary;
  static Color tertiary = AppTheme.getColorScheme.tertiary;
  static Color error = AppTheme.getColorScheme.error;
  static Color onError = AppTheme.getColorScheme.errorContainer;
  static Color grad1 = const Color(0xff1f245f);
  static Color grad2 = const Color(0xff125091);
  static Color grad3 = const Color(0xff0684db);
  static Color grad4 = const Color(0xff0367AC);
  static Color darkGrey = const Color(0xff707070);
  static Color lightGrey = const Color(0xffD5D6D6);
  static Color textFieldFocused = const Color(0xff017EFA);
  static Color textField = const Color(0xffB8C5D3);
  static Color black = Colors.black;
  static Color white = Colors.white;
}
