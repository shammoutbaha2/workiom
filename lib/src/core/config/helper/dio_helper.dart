import 'dart:convert';

import 'package:dio/dio.dart';

import '../../constant/network.dart';

abstract class DioHelper {
  Future<Response> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });

  Future<Response> post({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  });
  Future<Response> update({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  });
  Future<Response> delete({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  });
}

class DioHelperImpl extends DioHelper {
  final Dio client;

  DioHelperImpl({
    required this.client,
  });

  @override
  Future<Response> get({
    required String path,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
  }) async {
    final response = await client.get(
      baseURL + path,
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  @override
  Future<Response> post({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  }) async {
    final response = await client.post(
      baseURL + path,
      data: body == null
          ? null
          : hasFile
              ? FormData.fromMap(body)
              : jsonEncode(body),
      queryParameters: queryParameters,
      options: Options(
          headers: getHeaders(
              extraHeaders: extraHeaders, auth: auth, hasFile: hasFile)),
    );
    return response;
  }

  @override
  Future<Response> update({
    required String path,
    Map<String, dynamic>? body,
    Map<String, dynamic>? queryParameters,
    Map<String, String>? extraHeaders,
    bool auth = true,
    bool hasFile = false,
  }) async {
    final response = await client.put(
      baseURL + path,
      data: body == null
          ? null
          : hasFile
              ? FormData.fromMap(body)
              : jsonEncode(body),
      queryParameters: queryParameters,
      options: Options(
          headers: getHeaders(
              extraHeaders: extraHeaders, auth: auth, hasFile: hasFile)),
    );
    return response;
  }

  @override
  Future<Response> delete(
      {required String path,
      Map<String, dynamic>? body,
      Map<String, dynamic>? queryParameters,
      Map<String, String>? extraHeaders,
      bool auth = true}) async {
    final response = await client.delete(
      baseURL + path,
      data: body == null ? null : FormData.fromMap(body),
      queryParameters: queryParameters,
      options:
          Options(headers: getHeaders(extraHeaders: extraHeaders, auth: auth)),
    );
    return response;
  }

  Map<String, String> getHeaders(
      {Map? extraHeaders, bool auth = true, bool hasFile = false}) {
    return {
      "Accept": hasFile ? "multipart/form-data" : "application/json",
      if (auth) "Authorization": "Bearer $token",
      ...?extraHeaders,
    };
  }

  String get baseURL {
    return NetworkConstant.api;
  }

  String get token {
    try {
      return "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJodHRwOi8vc2NoZW1hcy54bWxzb2FwLm9yZy93cy8yMDA1LzA1L2lkZW50aXR5L2NsYWltcy9uYW1laWRlbnRpZmllciI6IjExMzQ5NCIsImh0dHA6Ly9zY2hlbWFzLnhtbHNvYXAub3JnL3dzLzIwMDUvMDUvaWRlbnRpdHkvY2xhaW1zL25hbWUiOiJzaGFtbW91dGJhaGFhMkBnbWFpbC5jb20iLCJBc3BOZXQuSWRlbnRpdHkuU2VjdXJpdHlTdGFtcCI6IkpCWVJCS0RNV0I3Uk9BWkxXQlJTUUpaS01aR1ZaRFNKIiwiaHR0cDovL3d3dy5hc3BuZXRib2lsZXJwbGF0ZS5jb20vaWRlbnRpdHkvY2xhaW1zL3RlbmFudElkIjoiMTAxMTQiLCJzdWIiOiIxMTM0OTQiLCJqdGkiOiJhNzEwNzgyYi03NjQzLTQ4NTQtOTRhZC0zYzA0ZDg4NDhlYTQiLCJpYXQiOjE2OTgxMzY1NTksInRva2VuX3ZhbGlkaXR5X2tleSI6IjVmNGZjZjc0LWM4NmYtNGVhNC04NzNjLTU4N2NmMzQ2M2NmOSIsInVzZXJfaWRlbnRpZmllciI6IjExMzQ5NEAxMDExNCIsInRva2VuX3R5cGUiOiIwIiwibmJmIjoxNjk4MTM2NTU5LCJleHAiOjE3MDA3Mjg1NTksImlzcyI6Ikxpc3R1cmUiLCJhdWQiOiJMaXN0dXJlIn0.pzppvj_s2hlXNpn31xJ6Cl0o1m1saCL-BeOFYhcK7xg";
    } catch (e) {
      return "";
    }
  }
}
