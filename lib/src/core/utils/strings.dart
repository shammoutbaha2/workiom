// ignore_for_file: non_constant_identifier_names, constant_identifier_names
class StringKeys {
  static const String TOKEN = "token";
  static const String REFRESH_TOKEN = "refreshToken";
  static const String NO_INTERNET_CONNECTION = 'No Internet Connection';
  static const String AN_ERROR_OCCURRED = 'an error occurred';
  static const String PLEASE_CHECK_YOUR_CONNECTION =
      'please check your connection and try later';
  static const String NOTHING_TO_SHOW = "Nothing To Show";

  static const String SERVER_ERROR = 'SERVER ERROR';
  static const String NETWORK_ERROR = 'NETWORK ERROR';
  static const String CACHE_ERROR = 'CACHE ERROR';
  static const String UNKNOWN_ERROR = 'UNKNOWN ERROR';
}
