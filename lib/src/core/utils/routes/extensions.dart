import 'package:flutter/material.dart';

import '../../config/size/size_utils.dart';

extension ScreenSize on num {
  double get gw => getHorizontalSize(toDouble());

  double get gh => getVerticalSize(toDouble());

  SizedBox get sizedH => SizedBox(height: gh);
  SizedBox get sizedW => SizedBox(width: gw);
}

extension ExtensionForWidget on Widget {
  Padding padding({double? all, double? hor, double? ver}) => Padding(
      padding: all != null
          ? EdgeInsets.all(all)
          : EdgeInsets.symmetric(horizontal: hor ?? 0, vertical: ver ?? 0),
      child: this);

  Widget sized({double? width, double? height, double? size}) => size != null
      ? SizedBox.square(
          dimension: size,
          child: this,
        )
      : SizedBox(width: width, height: height, child: this);

  InkWell onTap(void Function()? onIt) => InkWell(onTap: onIt, child: this);
}
