import 'package:get/get.dart';

import '../../../modules/dashboard/presentation/view/dashboard_screen.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: DashboardScreen.route,
      page: () => const DashboardScreen(),
      transition: Transition.cupertino,
    ),
  ];
}
