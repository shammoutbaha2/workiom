import '../../modules/dashboard/domain/models/dashboard_model.dart';

int getMaxNumberOfDashboards(DashboardWidgetDetailsModel details) {
  try {
    double number = 1;
    for (var i = 0; i < details.values!.length; i++) {
      for (var j = 0; j < details.values![i].value!.length; j++) {
        if (details.values![i].value![j].value! > number) {
          number = details.values![i].value![j].value!;
        }
      }
    }
    return (number ~/ 3 + 1).toInt();
  } on Exception {
    return 10;
  }
}

int getMaxNumberOfDashboardsWithSum(DashboardWidgetDetailsModel details) {
  try {
    double number = 1;
    for (var element in details.mainGroupLabelValues!) {
      double sum = 0;
      for (var i = 0; i < details.values!.length; i++) {
        for (var j = 0; j < details.values![i].value!.length; j++) {
          if (details.values![i].value![j].ids!
              .contains(element.value?.id ?? "sss")) {
            sum += details.values![i].value![j].value;
          }
        }
      }
      if (sum > number) number = sum;
    }
    return number.toInt();
  } on Exception {
    return 10;
  }
}

int getNumberOfValue(DashboardWidgetDetailsModel details, int i) {
  try {
    double number = 0;

    for (var j = 0; j < details.values![i].value!.length; j++) {
      if (details.values![i].value![j].value > 0) {
        number++;
      }
    }
    return number.toInt();
  } on Exception {
    return 10;
  }
}
