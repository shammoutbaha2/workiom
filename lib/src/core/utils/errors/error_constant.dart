// ignore_for_file: non_constant_identifier_names

import 'package:dio/dio.dart';

import '../../base/base_api_response.dart';
import '../../base/base_error_model.dart';
import '../strings.dart';
import 'error_hdl.dart';

class ErrorsConstantModel {
  static ResponseModel get NO_INTERNET_MODEL_ERROR => ResponseModel(
      errorModel: ErrorModel(StringKeys.NO_INTERNET_CONNECTION,
          type: StringKeys.NETWORK_ERROR));
  static ResponseModel DIO_MODEL_ERROR(DioException e) => ResponseModel(
      errorModel: ErrorModel(handleError(e), type: StringKeys.SERVER_ERROR));
  static ResponseModel GENERAL_MODEL_ERROR(e) => ResponseModel(
      errorModel: ErrorModel(e.toString(), type: StringKeys.SERVER_ERROR));
}
