import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import 'core/bindings/app_bindings.dart';
import 'core/utils/routes/app_pages.dart';
import 'modules/dashboard/presentation/view/dashboard_screen.dart';

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: GetMaterialApp(
          // theme: AppTheme.base(),
          debugShowCheckedModeBanner: false,
          // translations: AppTrans(),
          theme: _buildTheme(Brightness.dark),
          builder: (context, child) {
            return MediaQuery(
                data: MediaQuery.of(context).copyWith(textScaleFactor: 1),
                child: child!);
          },
          getPages: AppPages.pages,
          initialRoute: DashboardScreen.route,
          initialBinding: AppBindings()),
    );
  }

  ThemeData _buildTheme(brightness) {
    var baseTheme = ThemeData(brightness: brightness);

    return baseTheme.copyWith(
      textTheme: GoogleFonts.rubikTextTheme(baseTheme.textTheme),
    );
  }
}
